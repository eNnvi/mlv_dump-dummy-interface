﻿Imports System.Text
Imports System.IO
Imports System.Xml




Public Class Form1



    Dim FilePointer As IO.FileInfo
    Dim lineCount As Integer = 0
    Dim outdata As String




    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim fd As OpenFileDialog = New OpenFileDialog()
        'Dim strFileName As String

        Dim initDir As String
        If TextBox1.Text <> "" Then
            initDir = TextBox1.Text
        Else
            initDir = "C:\"
        End If

        fd.Title = "Open File Dialog"
        fd.InitialDirectory = initDir
        fd.Filter = "MLV files (*.mlv)|*.mlv"
        fd.FilterIndex = 2
        fd.RestoreDirectory = True

        If fd.ShowDialog() = DialogResult.OK Then
            TextBox1.Text = fd.FileName
        End If

    End Sub



    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim FolderBrowserDialog1 As FolderBrowserDialog = New FolderBrowserDialog()

        Dim initDir As String
        If TextBox2.Text <> "" Then
            initDir = TextBox2.Text
        Else
            initDir = "C:\"
        End If

        FolderBrowserDialog1.ShowNewFolderButton = True
        FolderBrowserDialog1.Description = "Choose DNG Output folder"
        FolderBrowserDialog1.SelectedPath = initDir

        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            TextBox2.Text = FolderBrowserDialog1.SelectedPath + ""
        End If



    End Sub

    Private Sub CheckBox4_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox4.CheckedChanged
        NumericUpDown1.Enabled = CheckBox4.Checked
        NumericUpDown2.Enabled = CheckBox4.Checked
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        FilePointer = New IO.FileInfo(TextBox1.Text)
        ListBox1.Items.Add("File information updated: " + FilePointer.Name)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        If TextBox1.Text = "" Then
            MsgBox("You have to select or drag/drop a file first", MsgBoxStyle.Critical)
            Return
        End If

        If System.IO.File.Exists(TextBox1.Text) <> True Then
            MsgBox("The path must be valid!", MsgBoxStyle.Critical)
            Return
        End If

        If IO.Path.GetExtension(TextBox1.Text).ToLower() <> ".mlv" Then
            MsgBox("Only MLV files are supported", MsgBoxStyle.Critical)
            Return
        End If

        If TextBox2.Text = "" Then
            MsgBox("Update folder will be choosen automagically", MsgBoxStyle.Information)
        End If

        CheckBox1.Enabled = False
        CheckBox2.Enabled = False
        CheckBox3.Enabled = False
        CheckBox4.Enabled = False

        RadioButton1.Enabled = False
        RadioButton2.Enabled = False
        RadioButton3.Enabled = False
        RadioButton4.Enabled = False

        TextBox1.Enabled = False
        TextBox2.Enabled = False

        NumericUpDown1.Enabled = False
        NumericUpDown2.Enabled = False

        Button1.Enabled = False
        Button2.Enabled = False
        Button3.Enabled = False


        Dim mlvdump As New Process()

        Dim inputfile As String = FilePointer.FullName
        Dim outputdir As String

        If TextBox2.Text = "" Then
            TextBox2.Text = FilePointer.DirectoryName
        End If

        outputdir = TextBox2.Text + "\" + System.IO.Path.GetFileNameWithoutExtension(FilePointer.FullName) + "_1_" + FilePointer.CreationTime.Year.ToString() + "-" + FilePointer.CreationTime.Month.ToString() + "-" + FilePointer.CreationTime.Day.ToString() + "_0001_C0000"

        ListBox1.Items.Add("Output will be at: " + outputdir)
        ListBox1.TopIndex = ListBox1.Items.Count - 1

        MkDir(outputdir)

        Dim fileprefix As String = outputdir + "\" + System.IO.Path.GetFileNameWithoutExtension(FilePointer.FullName) + "_1_" + FilePointer.CreationTime.Year.ToString() + "-" + FilePointer.CreationTime.Month.ToString() + "-" + FilePointer.CreationTime.Day.ToString() + "_0001_C0000" + "_"

        Dim args As String = "--dng"
        args = args + If(RadioButton1.Checked = True, " --no-cs", "")
        args = args + If(RadioButton2.Checked = True, " --cs2x2", "")
        args = args + If(RadioButton3.Checked = True, " --cs3x3", "")
        args = args + If(RadioButton4.Checked = True, " --cs5x5", "")

        args = args + If(CheckBox1.Checked = True, " --no-fixcp", "")
        args = args + If(CheckBox2.Checked = True, " --fixcp2", "")
        args = args + If(CheckBox3.Checked = True, " --no-stripes", "")

        If CheckBox4.Checked = True Then ' Advanced cfg: B&W Level
            args += " --black-fix=" + NumericUpDown1.Value.ToString()
            args += " --white-fix=" + NumericUpDown2.Value.ToString()
        End If

        args += " -o " + Chr(34).ToString() + fileprefix + Chr(34)

        mlvdump.StartInfo.FileName = "mlv_dump.exe"
        mlvdump.StartInfo.Arguments = "--batch " + args + " " + Chr(34).ToString() + TextBox1.Text + Chr(34).ToString()
        mlvdump.StartInfo.CreateNoWindow = True
        mlvdump.StartInfo.UseShellExecute = False
        mlvdump.StartInfo.RedirectStandardOutput = True

        AddHandler mlvdump.OutputDataReceived, AddressOf OutputHandler

        ' MsgBox(mlvdump.StartInfo.Arguments)

        mlvdump.Start()
        mlvdump.BeginOutputReadLine()

        While mlvdump.HasExited = False

        End While

        mlvdump.Close()

        CheckBox1.Enabled = True
        CheckBox2.Enabled = True
        CheckBox3.Enabled = True
        CheckBox4.Enabled = True

        RadioButton1.Enabled = True
        RadioButton2.Enabled = True
        RadioButton3.Enabled = True
        RadioButton4.Enabled = True

        TextBox1.Enabled = True
        TextBox2.Enabled = True

        NumericUpDown1.Enabled = CheckBox4.Checked
        NumericUpDown2.Enabled = CheckBox4.Checked

        Button1.Enabled = True
        Button2.Enabled = True
        Button3.Enabled = True


    End Sub

    Private Sub Me_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Me.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.All
        End If
    End Sub

    Private Sub Me_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Me.DragDrop
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            Dim MyFiles() As String
            'Dim i As Integer

            ' Assign the files to an array.
            MyFiles = e.Data.GetData(DataFormats.FileDrop)
            ' Loop through the array and add the files to the list.
            If MyFiles.Length > 1 Then
                MsgBox("Only 1 file at the time")
                Return
            End If


            If IO.Path.GetExtension(MyFiles(0)).ToLower() <> ".mlv" Then
                MsgBox("Only MLV files are supported")
                Return
            End If

            ListBox1.Items.Add("Trovato drop: " + MyFiles(0))
            ListBox1.TopIndex = ListBox1.Items.Count - 1
            TextBox1.Text = MyFiles(0)
        End If
    End Sub

    Sub OutputHandler(sender As Object, e As DataReceivedEventArgs)
        If Not String.IsNullOrEmpty(e.Data) Then
            lineCount += 1

            'MsgBox(e.Data)
            Console.WriteLine("[" + lineCount.ToString() + "]: " + e.Data)
            If e.Data.StartsWith("[P]") Then
                Dim prog As String() = e.Data.Split(" ")
                Dim blocks = prog(1)
                Dim vid = prog(2)
                Dim aud = prog(3)

                Dim progress_split = vid.Substring(2).Split("/")
                Dim audio_split = aud.Substring(2).Split("/")
                Dim advance As Integer = (Integer.Parse(progress_split(0)) + Integer.Parse(audio_split(0)) + 1) / (Integer.Parse(progress_split(1)) + Integer.Parse(audio_split(1)) + 1) * 100

                UpdateProgress(advance)
            End If
        End If
    End Sub


    Delegate Sub StringArgReturningVoidDelegate([text] As String)
    Private Sub SetText(ByVal [text] As String)

        ' InvokeRequired required compares the thread ID of the  
        ' calling thread to the thread ID of the creating thread.  
        ' If these threads are different, it returns true.  
        If Me.TextBox1.InvokeRequired Then
            Dim d As New StringArgReturningVoidDelegate(AddressOf SetText)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.ListBox1.Items.Add([text])
            ListBox1.TopIndex = ListBox1.Items.Count - 1
        End If
    End Sub


    Delegate Sub StringArgReturningVoidDelegatePB([text] As Integer)
    Private Sub UpdateProgress(ByVal [text] As Integer)

        ' InvokeRequired required compares the thread ID of the  
        ' calling thread to the thread ID of the creating thread.  
        ' If these threads are different, it returns true.  
        If Me.ProgressBar1.InvokeRequired Then
            Dim d As New StringArgReturningVoidDelegatePB(AddressOf UpdateProgress)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.ProgressBar1.Value = [text]
        End If
    End Sub


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Update_preset()

        Dim arguments As String() = Environment.GetCommandLineArgs()

        If arguments.Count() < 2 Then
            Return
        End If


        If File.Exists(arguments(1)) Then
            TextBox1.Text = arguments(1)
        End If
    End Sub

    Private Sub Update_preset()
        ComboBox1.Items.Clear()
        ComboBox1.Items.Add("")


        Dim xr As XmlDocument = New XmlDocument()
        xr.Load(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\MLV Dump\presets.xml")
        ' MsgBox(Path.GetDirectoryName(Application.ExecutablePath))

        Dim presets As XmlNodeList = xr.SelectNodes("/collection/preset")
        Dim node As XmlNode

        For Each node In presets
            If node Is Nothing Then
                MsgBox("Error")
                Continue For
            End If
            If node.Attributes.GetNamedItem("title") Is Nothing Then
                MsgBox("No way")
                Continue For
            End If
            Dim title As String = node.Attributes.GetNamedItem("title").Value
            If title IsNot Nothing Then
                ComboBox1.Items.Add(title)
            Else
                MsgBox("Nothing found")
            End If
        Next

        'Dim document As XmlReader = New XmlTextReader(Path.GetDirectoryName(Application.ExecutablePath) + "\presets.xml")

        ''loop through the xml file
        'While document.Read()
        '    If document.NodeType = XmlNodeType.Element Then
        '        If (document.Name = "preset") Then

        '            MsgBox(document.ReadInnerXml.ToString())

        '        End If
        '    End If

        'End While
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        If ComboBox1.Text = "" Then
            CheckBox1.Checked = False
            CheckBox2.Checked = False
            CheckBox3.Checked = False
            CheckBox4.Checked = False

            RadioButton1.Checked = True
            RadioButton2.Checked = False
            RadioButton3.Checked = False
            RadioButton4.Checked = False

            NumericUpDown1.Value = 2048
            NumericUpDown1.Value = 15000
            Return
        End If

        Dim xmlDoc As XmlDocument = New XmlDocument()
        xmlDoc.Load(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\MLV Dump\presets.xml")
        Dim node As XmlNode = xmlDoc.SelectSingleNode("/collection/preset[@title = '" + ComboBox1.Text + "']")
        CheckBox1.Checked = If(node.SelectSingleNode("nofixcp").InnerText = 1, True, False)
        CheckBox2.Checked = If(node.SelectSingleNode("fixcp2").InnerText = 1, True, False)
        CheckBox3.Checked = If(node.SelectSingleNode("nostripes").InnerText = 1, True, False)
        CheckBox4.Checked = If(node.SelectSingleNode("advanced").InnerText = 1, True, False)

        RadioButton1.Checked = If(node.SelectSingleNode("cs").InnerText = 0, True, False)
        RadioButton2.Checked = If(node.SelectSingleNode("cs").InnerText = 1, True, False)
        RadioButton3.Checked = If(node.SelectSingleNode("cs").InnerText = 2, True, False)
        RadioButton4.Checked = If(node.SelectSingleNode("cs").InnerText = 3, True, False)

        NumericUpDown1.Value = Integer.Parse(node.SelectSingleNode("black").InnerText)
        NumericUpDown1.Value = Integer.Parse(node.SelectSingleNode("white").InnerText)

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim presName As String = InputBox("Enter name of new preset", "Enter name", "")
        If presName.Length = 0 Then
            MsgBox("No preset has been saved")
            Return
        End If

        ' fast way to check if exists
        If ComboBox1.Items.Contains(presName) Then
            MsgBox("This preset already exists")
            Return
        End If

        Dim xmlDoc As XmlDocument = New XmlDocument()
        xmlDoc.Load(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\MLV Dump\presets.xml")
        Dim node As XmlWriter = xmlDoc.SelectSingleNode("/collection").CreateNavigator().AppendChild()

        With node
            .WriteStartElement("preset")
            .WriteAttributeString("title", presName)
            .WriteElementString("cs", If(RadioButton1.Checked, "0", If(RadioButton2.Checked, "1", If(RadioButton3.Checked, "2", "3"))))
            .WriteElementString("nofixcp", If(CheckBox1.Checked, "1", "0"))
            .WriteElementString("fixcp2", If(CheckBox2.Checked, "1", "0"))
            .WriteElementString("nostripes", If(CheckBox3.Checked, "1", "0"))
            .WriteElementString("advanced", If(CheckBox4.Checked, "1", "0"))
            .WriteElementString("black", NumericUpDown1.Value.ToString())
            .WriteElementString("white", NumericUpDown1.Value.ToString())
            .WriteEndElement()
        End With

        node.Close()

        xmlDoc.Save(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\MLV Dump\presets.xml")

        MsgBox("Preset  saved")
        Update_preset()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If ComboBox1.Text = "" Then
            MsgBox("Couldn't delete this preset")
            Return
        End If

        Dim result As Integer = MessageBox.Show("Are you sure you want to delete this preset?", "Attention", MessageBoxButtons.YesNo)
        If result = DialogResult.No Then
            Return
        End If

        Dim xmlDoc As XmlDocument = New XmlDocument()
        xmlDoc.Load(Path.GetDirectoryName(Application.ExecutablePath) + "\presets.xml")
        Dim node As XmlNode = xmlDoc.SelectSingleNode("/collection/preset[@title = '" + ComboBox1.Text + "']")

        xmlDoc.SelectSingleNode("/collection").RemoveChild(node)


        xmlDoc.Save(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\MLV Dump\presets.xml")

        MsgBox("Preset deleted")
        Update_preset()
        ComboBox1.Text = ""
    End Sub
End Class
